package com.example.discussion.models;

import javax.persistence.*;

//a. Create a User class.
@Entity(name="user")
@Table(name="users")
public class User {
    //b. Add the User properties
    //i.Id
    @Id
    @GeneratedValue
    private Long id;
    //ii.Username
    @Column
    private String username;
    //iii.Password
    @Column
    private String password;
    //Constructors
    public User(){};

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }
    //Setters and Getters
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
