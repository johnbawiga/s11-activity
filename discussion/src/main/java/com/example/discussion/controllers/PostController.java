package com.example.discussion.controllers;

import com.example.discussion.models.Post;
import com.example.discussion.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

//handles incoming HTTP requests and map them to the corresponding methods in the controller
@RestController
//CORS - Cross Origin Resource Sharing
//Allows requests from a different domain to be made to the application
@CrossOrigin
public class PostController {
    @Autowired
    PostService postService;

    //Creating a Post
    @RequestMapping(value = "/posts",method = RequestMethod.POST)
    public ResponseEntity<Object> createPost(@RequestBody Post post){
        postService.createPost(post);
        return new ResponseEntity<>("Post created successfully!", HttpStatus.CREATED);
    }
    //Getting all posts
    @RequestMapping(value="/posts",method=RequestMethod.GET)
    public ResponseEntity<Object> getPosts(){
        return new ResponseEntity<>(postService.getPost(),HttpStatus.OK);
    }
    //Deleting a post
    @RequestMapping(value="/posts/{postid}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deletePost(@PathVariable Long postid){
        return postService.deletePost(postid);
    }
    //Update a post
    @RequestMapping(value="/posts/{postid}", method=RequestMethod.PUT)
    public ResponseEntity<Object> updatePost(@PathVariable Long postid, @RequestBody Post post){
        return postService.updatePost(postid,post);
    }
}
